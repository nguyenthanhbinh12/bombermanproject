package uet.oop.bomberman;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import uet.oop.bomberman.entities.*;
//import uet.oop.bomberman.entities.enemies.Kondoria;
import uet.oop.bomberman.entities.items.*;
import uet.oop.bomberman.entities.objects.*;
import uet.oop.bomberman.entities.Bomber;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.enemies.Balloon;
import uet.oop.bomberman.entities.enemies.Doll;
import uet.oop.bomberman.entities.abstracts.Enemy;
import uet.oop.bomberman.entities.enemies.Oneal;
import uet.oop.bomberman.entities.abstracts.Item;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sounds.PlaySound;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Scanner;
import javafx.scene.input.KeyEvent;

public class BombermanGame extends Application {

    public static final int WIDTH = 31;
    public static final int HEIGHT = 13;

    public static GraphicsContext gc;
    private Canvas canvas;

    public static List<Entity> entities = new ArrayList<>();
    public static List<Entity> fixedObjects = new ArrayList<>(); // contains Grass and Walls
    public static List<Entity> unfixedObjects = new ArrayList<>(); // contains Items and Bricks
    public static List<Entity> flames = new ArrayList<>();
    public static List<Entity> damagedEntities = new ArrayList<>();

    public static String[] Gamemap;
    public static Direct direction = new Direct();
    public static Entity bomberman = new Bomber(1, 1, Sprite.player_right.getFxImage());
    public static int creep = 0;

    public static void main(String[] args) {
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        // Tao Canvas
        canvas = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        gc = canvas.getGraphicsContext2D();

        // Tao root container
        Group root = new Group();
        root.getChildren().add(canvas);

        // Tao scene
        Scene scene = new Scene(root);

        // Them scene vao stage
        stage.setScene(scene);
        stage.show();


        //loop
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                update();
                render();
            }
        };

        timer.start();


        //khoi tao map
        Gamemap = createMap();
        entities.add(bomberman);
        PlaySound.playBackgroundMusic(PlaySound.MAIN_MENU, true);

        // nhan key tu ban phim
        setKey(scene);
    }


    public void setKey(Scene scene) {
        scene.setOnKeyPressed((KeyEvent keyEvent) -> {
            if (null != keyEvent.getCode()) switch (keyEvent.getCode()) {
                case LEFT:
                    direction.left = true;
                    break;
                case RIGHT:
                    direction.right = true;
                    break;
                case UP:
                    direction.up = true;
                    break;
                case DOWN:
                    direction.down = true;
                    break;
                case SPACE:
                    int curX = (int) Math.round(bomberman.getX());
                    int curY = (int) Math.round(bomberman.getY());
                    if (Gamemap[curY].charAt(curX) != 't') {
                        Bomber bomber = (Bomber) bomberman;
                        if (bomber.isLive() && bomber.bombCounter() < bomber.bombCount) {
                            Entity bomb = bomber.placeBomb();
                            entities.add(bomb);
                            flames.addAll(((Bomb) bomb).getFlames());
                        }
                    }   break;
                default:
                    break;
            }
        });
        scene.setOnKeyReleased((KeyEvent keyEvent) -> {
            if (null != keyEvent.getCode()) switch (keyEvent.getCode()) {
                case LEFT:
                    direction.left = false;
                    break;
                case RIGHT:
                    direction.right = false;
                    break;
                case UP:
                    direction.up = false;
                    break;
                case DOWN:
                    direction.down = false;
                    break;
                default:
                    break;
            }
        });
    }

    //ham khoi tao map
    public String[] createMap() {
        try {
            Scanner scanner = new Scanner(new BufferedReader(new FileReader("res/levels/Level1.txt")));
            int level = scanner.nextInt();
            int row = scanner.nextInt();
            int column = scanner.nextInt();

            String[] Gamemap = new String[row];

            String s = scanner.nextLine();

            for (int i = 0; i < row; i++) {
                Gamemap[i] = scanner.nextLine();
                for (int j = 0; j < column; j++) {
                    Entity object;
                    char key = Gamemap[i].charAt(j);
                    switch (key) {
                        case '#':
                            object = new Wall(j, i, Sprite.wall.getFxImage());
                            fixedObjects.add(object);
                            break;
                        case '*':
                            fixedObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage()));
                            break;
                        case '1':
                            fixedObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            entities.add(new Balloon(j, i, Sprite.balloom_left1.getFxImage()));
                            ++creep;
                            break;
                        case '2':
                            fixedObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            entities.add(new Oneal(j, i, Sprite.oneal_left1.getFxImage()));
                            ++creep;
                            break;
                        case '3':
                            fixedObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            entities.add(new Doll(j, i, Sprite.doll_right1.getFxImage()));
                            ++creep;
                            break;
                        case 'b':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemBomb(j, i, Sprite.powerup_bombs.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        case 'f':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemFlame(j, i, Sprite.powerup_flames.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        case 's':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemSpeed(j, i, Sprite.powerup_speed.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        case 'h':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemHeart(j, i, Sprite.powerup_detonator.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        case 'w':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemWallPass(j, i, Sprite.powerup_wallpass.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        case 'x':
                            unfixedObjects.add(new Brick(j, i, Sprite.brick.getFxImage(), new ItemPortal(j, i, Sprite.portal.getFxImage())));
                            Gamemap[i] = Gamemap[i].substring(0, j) + '*' + Gamemap[i].substring(j + 1);
                            break;
                        default:
                            fixedObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            break;
                    }
                }
            }
            return Gamemap;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            return null;
        }
    }

    public void restart() {
        entities.clear();
        fixedObjects.clear();
        unfixedObjects.clear();
        flames.clear();
        damagedEntities.clear();

        bomberman = new Bomber(1, 1, Sprite.player_right.getFxImage());
        entities.add(bomberman);
        Gamemap = createMap();
    }
    public void update() {
        Bomber bomber = (Bomber) bomberman;
        //Item obj = null;

        if (!bomber.isLive() && flames.isEmpty()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            restart();
            PlaySound.playBackgroundMusic(PlaySound.MAIN_MENU, true);
        }

        entities.forEach(Entity::update);
        flames.forEach(Entity::update);
        updateDamagedObjects();
        updateItem();
    }

    public void render() {
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        fixedObjects.forEach(g -> g.render(gc));
        unfixedObjects.forEach(g -> g.render(gc));
        entities.forEach(g -> g.render(gc));
        damagedEntities.forEach(g -> g.render(gc));
        flames.forEach(g -> g.render(gc));
    }



    public void updateStaticObjectsAndEnemies(Entity br) {
        if (br instanceof Brick) {
            if (((Brick) br).isDone()) {

                Brick brick = (Brick) br;

                damagedEntities.remove(br);
                unfixedObjects.remove(br);
                Entity entity = new Grass((int) br.getX(), (int) br.getY(), Sprite.grass.getFxImage());
                if (brick.getItem() != null) {
                    if (brick.getItem() instanceof ItemBomb) {
                        entity = new ItemBomb((int) br.getX(), (int) br.getY(), Sprite.powerup_bombs.getFxImage());
                        unfixedObjects.add(entity);
                    } else if (brick.getItem() instanceof ItemSpeed) {
                        entity = new ItemSpeed((int) br.getX(), (int) br.getY(), Sprite.powerup_speed.getFxImage());
                        unfixedObjects.add(entity);
                    } else if (brick.getItem() instanceof ItemFlame) {
                        entity = new ItemFlame((int) br.getX(), (int) br.getY(), Sprite.powerup_flamepass.getFxImage());
                        unfixedObjects.add(entity);
                    } else if (brick.getItem() instanceof ItemHeart) {
                        entity = new ItemHeart((int) br.getX(), (int) br.getY(), Sprite.powerup_detonator.getFxImage());
                        unfixedObjects.add(entity);
                    } else if (brick.getItem() instanceof ItemWallPass) {
                        entity = new ItemWallPass((int) br.getX(), (int) br.getY(), Sprite.powerup_wallpass.getFxImage());
                        unfixedObjects.add(entity);
                    } else if (brick.getItem() instanceof ItemPortal) {
                        Entity portal1 = new ItemPortal((int) br.getX(), (int) br.getY(), Sprite.portal.getFxImage());
                        fixedObjects.add(portal1);
                    }

                } else {
                    fixedObjects.add(entity);
                }

                int getX = (int) br.getX();
                int getY = (int) br.getY();
                Gamemap[getY] = Gamemap[getY].substring(0, getX) + " " + Gamemap[getY].substring(getX + 1);

            } else {
                br.update();
            }
        } else if (br instanceof Enemy) {
            if (br.getImg() == null) {
                entities.remove(br);
                --creep;
            }
        }
    }

    public void checkForDamagedEntities(Entity entity) {
        if (entity instanceof Bomb) {
            Bomb bomb = (Bomb) entity;
            if ((bomb).isExploded()) {

                int getX = (int) bomb.getX();
                int getY = (int) bomb.getY();
                Gamemap[getY] = Gamemap[getY].substring(0, getX) + " " + Gamemap[getY].substring(getX + 1);

                (bomb).handleFlameCollision(entities, unfixedObjects, damagedEntities);
                System.out.println(entities.remove(bomb));
            }
        }
    }

    public void updateDamagedObjects() {
        try {
            entities.forEach(entity -> {
                checkForDamagedEntities(entity);
                damagedEntities.forEach(this::updateStaticObjectsAndEnemies);

            });
            flames.forEach(entity -> {
                if (entity instanceof Flame) {
                    if (((Flame) entity).isDone()) {
                        flames.remove(entity);
                    }
                }
            });
        } catch (ConcurrentModificationException e) {

        }

        if (bomberman instanceof Bomber) {
            Bomber bomber = (Bomber) bomberman;
            if (bomber.collision(entities)) {
                bomber.dieImg();
                bomber.setLive(false);
            }
        }
    }

    public void updateItem() {
        int size = unfixedObjects.size();
        for (int i = 0; i < size; i++) {
            Entity entity = unfixedObjects.get(i);
            if (entity instanceof Item) {
                if (((Item) entity).collision(bomberman)) {
                    Bomber bomber = (Bomber) bomberman;
                    if (entity instanceof ItemSpeed) {
                        bomber.setSpeed(bomber.getSpeed() + 0.01);
                    }
                    else if (entity instanceof ItemFlame) {
                        bomber.setBombRange(bomber.getBombRange() + 1);
                    }
                    else if (entity instanceof ItemBomb){
                        bomber.setBombCount(bomber.getBombCount() + 1);
                    }
                    else if (entity instanceof ItemWallPass){
                        bomber.throughWall = true;
                        bomber.setThroughWall(bomber.isThroughWall());
                    }
                    else if (entity instanceof ItemHeart){
                        bomber.shield = true;
                        bomber.setShield(bomber.isShield());
                    }
                    unfixedObjects.remove(i);
                    PlaySound.powerUp.play();
                    Entity grass = new Grass((int) entity.getX(), (int) entity.getY(), Sprite.grass.getFxImage());
                    fixedObjects.add(grass);
                    i--;
                    size--;
                }
            }
        }
    }
}
