package uet.oop.bomberman.entities;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.abstracts.Enemy;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.entities.objects.Bomb;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sounds.PlaySound;

import java.util.ArrayList;
import java.util.List;

public class Bomber extends Entity {

    // ability
    public List<Bomb> bombList = new ArrayList<>();
    private int bombRange = 2;
    public int bombCount = 1;
    public double speed = 0.08;
    private int deathCountDown = 3;
    private boolean live = true;
    public boolean throughWall = false;
    public boolean shield = false;

    /*a
    For rendering
     */
    private final Status status = new Status();
    private final Direct direction = BombermanGame.direction;


    public Bomber(double x, double y, Image img) {
        super(x, y, img);
        rectangle = new Rectangle(x, y, 0.675, 0.75);
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public boolean isLive() {
        return live;
    }

    public void setShield(boolean shield) {
        this.shield = shield;
    }

    public boolean isShield() {
        return shield;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setThroughWall(boolean throughWall) {
        this.throughWall = throughWall;
    }

    public boolean isThroughWall() {
        return throughWall;
    }

    public int getHeart() {
        return deathCountDown;
    }

    public void setHeart(int deathCountDown) {
        this.deathCountDown = deathCountDown;
    }

    public int getBombCount() {
        return bombCount;
    }

    public void setBombCount(int bombCount) {
        this.bombCount = bombCount;
    }

    public int getBombRange() {
        return bombRange;
    }

    public void setBombRange(int bombRange) {
        this.bombRange = bombRange;
    }

    @Override
    public void setLocation(double x, double y) {
        rectangle.setX(x);
        rectangle.setY(y);
    }

    public static int round(double x) {
        double x_ = Math.floor(x);
        if (x - x_ < 0.32) return (int) x_;
        if (x - x_ > 0.7) return (int) (x_ + 1);
        else return -1;

    }


    @Override
    public boolean moveLeft() {
        img = Sprite.movingSprite(Sprite.player_left, Sprite.player_left_1, Sprite.player_left_2, status.left).getFxImage();
        if (status.left == 8) {
            status.left = 0;
        } else {
            status.left++;
        }
        if (x - speed >= Math.floor(x)) {
            x -= speed;
            setLocation(x, y);
            return true;
        } else {
            int x1 = Bomber.round(x);
            int y1 = Bomber.round(y);
            if (x1 == -1 || y1 == -1) {
                return false;
            } else {
                if (throughWall == false) {
                    if (BombermanGame.Gamemap[y1].charAt(x1 - 1) != '#' &&
                            BombermanGame.Gamemap[y1].charAt(x1 - 1) != '*' &&
                            BombermanGame.Gamemap[y1].charAt(x1 - 1) != 't') {
                        y = y1;
                        x = x - speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                } else {
                        if (BombermanGame.Gamemap[y1].charAt(x1 - 1) != '#' &&
                                BombermanGame.Gamemap[y1].charAt(x1 - 1) != 't') {
                            y = y1;
                            x = x - speed;
                            setLocation(x, y);
                            PlaySound.walk.play();
                            return true;
                        }

                    }

                }
            }
        //        PlaySound.walk.play();
        return true;
    }

    @Override
    public boolean moveRight() {
        img = Sprite.movingSprite(Sprite.player_right, Sprite.player_right_1, Sprite.player_right_2, status.right).getFxImage();
        if (status.right == 8) {
            status.right = 0;
        } else {
            status.right++;
        }
        if (x + 0.6875 + speed <= Math.floor(x + 0.6875) + 1) {
            x += speed;
            setLocation(x, y);
            return true;
        } else {
            int x1 = Bomber.round(x + 0.6875);
            int y1 = Bomber.round(y);
            if (y1 == -1 || x1 == -1) {
                return false;
            } else {
                if (throughWall==false){
                    if (BombermanGame.Gamemap[y1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != '*' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != 't') {
                        y = y1;
                        x = x + speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                } else if (throughWall==true){
                    if (BombermanGame.Gamemap[y1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != 't') {
                        y = y1;
                        x = x + speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                }
            }

        }
//        PlaySound.walk.play();
        return true;
    }

    @Override
    public boolean moveUp() {
        img = Sprite.movingSprite(Sprite.player_up, Sprite.player_up_1, Sprite.player_up_2, status.up).getFxImage();
        if (status.up == 8) {
            status.up = 0;
        } else {
            status.up++;
        }
        if (y - speed >= Math.floor(y)) {
            y -= speed;
            setLocation(x, y);
            return true;
        } else {
            int x1 = Bomber.round(x);
            int y1 = Bomber.round(y);
            if (x1 == -1 || y1 == -1) {
                return false;
            } else {
                if (throughWall == false) {
                    if (BombermanGame.Gamemap[y1 - 1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1 - 1].charAt(x1) != '*' &&
                            BombermanGame.Gamemap[y1 - 1].charAt(x1) != 't') {
                        double x_ = x - Math.floor(x);
                        if (x_ <= 0.3 && x_ >= 0.1) x = x1 + 0.24;
                        else if ((x_ >= 0.7)) x = x1;
                        y -= speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                } else {
                    if (BombermanGame.Gamemap[y1 - 1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1 - 1].charAt(x1) != 't') {
                        double x_ = x - Math.floor(x);
                        if (x_ <= 0.3 && x_ >= 0.1) x = x1 + 0.24;
                        else if ((x_ >= 0.7)) x = x1;
                        y -= speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                }
            }
        }
        //        PlaySound.walk.play();
        return true;
    }

    @Override
    public boolean moveDown() {
        img = Sprite.movingSprite(Sprite.player_down, Sprite.player_down_1, Sprite.player_down_2, status.down).getFxImage();
        if (status.down == 8) {
            status.down = 0;
        } else {
            status.down++;
        }
        if (y + 1 + speed <= Math.ceil(y + 1)) {
            y += speed;
            setLocation(x, y);
            return true;
        } else {
            int x1 = Bomber.round(x);
            int y1 = Bomber.round(y + 1);
            if (y1 == -1 || x1 == -1) {
                return false;
            } else {
                if (throughWall == false) {
                    if (BombermanGame.Gamemap[y1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != '*' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != 't') {
                        double x_ = x - Math.floor(x);
                        if (x_ <= 0.3 && x_ >= 0.1) x = x1 + 0.24;
                        else if ((x_ >= 0.7)) x = x1;
                        y += speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                } else {
                    if (BombermanGame.Gamemap[y1].charAt(x1) != '#' &&
                            BombermanGame.Gamemap[y1].charAt(x1) != 't') {
                        double x_ = x - Math.floor(x);
                        if (x_ <= 0.3 && x_ >= 0.1) x = x1 + 0.24;
                        else if ((x_ >= 0.7)) x = x1;
                        y += speed;
                        setLocation(x, y);
                        PlaySound.walk.play();
                        return true;
                    }
                }
            }
        }
//        PlaySound.walk.play();
        return true;
    }

    public Entity placeBomb() {
        Entity bom = new Bomb((int) Math.round(this.getX()), (int) Math.round(this.getY()), Sprite.bomb.getFxImage(), this.bombRange);
        this.bombList.add((Bomb) bom);
        PlaySound.plantBomb.play();
        return bom;
    }

    public int bombCounter() {
        return bombList.size();
    }

    public void dieImg() {
        if (deathCountDown == 0) {
            //live=false;
            this.img = null;
        } else {
            //if (shield == false) {
                this.img = Sprite.bombExplodeSprite(Sprite.player_dead3, Sprite.player_dead2, Sprite.player_dead1, deathCountDown).getFxImage();
                deathCountDown--;
                PlaySound.playBackgroundMusic(PlaySound.LIFE_LOST, false);

        }
    }

    @Override
    public void update() {
        if (live) {
            if (direction.left && !direction.up && !direction.down) moveLeft();
            if (direction.right && !direction.up && !direction.down) moveRight();
            if (direction.up && !direction.right && !direction.left) moveUp();
            if (direction.down && !direction.right && !direction.left) moveDown();
            if (!bombList.isEmpty()) {
                if (bombList.get(bombList.size() - 1).isExploded()) {
                    bombList.remove(bombList.size() - 1);
                    PlaySound.explosion.play();
                }
            }
        } else {
            dieImg();
        }
    }

    public boolean collision(List<Entity> entities) {

        return entities.stream().filter(x -> (x instanceof Enemy)).anyMatch(x -> (this.rectangle.intersects(x.rectangle.getLayoutBounds())));
    }
}
