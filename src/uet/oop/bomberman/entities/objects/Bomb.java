package uet.oop.bomberman.entities.objects;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.Bomber;
import uet.oop.bomberman.entities.abstracts.Enemy;
import uet.oop.bomberman.entities.abstracts.Entity;
import uet.oop.bomberman.graphics.Sprite;

import java.util.ArrayList;
import java.util.List;

public class Bomb extends Entity {

    private int bombLevel = 3;
    private List<Entity> flames = new ArrayList<>();
    private boolean done = false;
    private boolean exploded = false;
    private int explosionCountDown = 15;
    private int tickingCountDown = 90;

    public List<Entity> getFlames() {
        return flames;
    }

    // set up flame cho phu hop


    public void setTickingCountDown(int tickingCountDown) {
        this.tickingCountDown = tickingCountDown;
    }

    public int getTickingCountDown() {
        return tickingCountDown;
    }

    public void setFlames() {
        String[] position = {"left", "down", "right", "top", "left_most", "down_most", "right_most", "top_most", "center" };
        int[] iX = {-1, 0, 1, 0};
        int[] iY = {0, 1, 0, -1};

        // let not bomber cross the bomb
        String map = BombermanGame.Gamemap[(int) y];
        BombermanGame.Gamemap[(int) y] = map.substring(0, (int) x) + "t" +
                map.substring((int) x + 1);

        flames.add(new Flame(x, y, null, "center"));
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= bombLevel; j++) {
                char flag = BombermanGame.Gamemap[(int) y + j * iY[i]].charAt((int) x + j * iX[i]);
                if (flag == '#' || flag == '*') {
                    if (flag == '*') {
                        String Gamemap = BombermanGame.Gamemap[(int) y + j * iY[i]];

                        // feature for chain explosion
                        BombermanGame.Gamemap[(int) y + j * iY[i]] = Gamemap.substring(0, (int) x + j * iX[i]) + "t" +
                                map.substring((int) x + j * iX[i] + 1);
                    }
                    break;
                }
                if (j == bombLevel) {
                    flames.add(new Flame(x + iX[i] * bombLevel, y + iY[i] * bombLevel, null, position[i + 4]));
                } else {
                    flames.add(new Flame(x + iX[i] * j, y + iY[i] * j, null, position[i]));
                }
            }
        }
    }

    public void setBombLevel(int bombLevel) {
        this.bombLevel = bombLevel;
    }

    public int getBombLevel() {
        return bombLevel;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isExploded() {
        return exploded;
    }

    public void setExploded(boolean exploded) {
        this.exploded = exploded;
    }

    public Bomb(int x, int y, Image img) {
        super(x, y, img);
        rectangle = new Rectangle(x, y, 0.99, 0.99);
        setFlames();
    }

    public Bomb(int x, int y, Image img, int bombLevel) {
        super(x, y, img);
        this.bombLevel = bombLevel;
        rectangle = new Rectangle(x, y, 0.99, 0.99);
        setFlames();
    }

    @Override
    public void update() {
        if (!isExploded()) {
            tickingImg();
        } else {
            String map = BombermanGame.Gamemap[(int) y];
            System.out.println(map);
            BombermanGame.Gamemap[(int) y] = map.substring(0, (int) x) + " " +
                    map.substring((int) x + 1);
            System.out.println(BombermanGame.Gamemap[(int) y]);
            explodingImg();

        }
    }

    public void tickingImg() {
        if (tickingCountDown == 0) {
            setExploded(true);
        } else {
            this.img = Sprite
                    .bombTickingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, tickingCountDown)
                    .getFxImage();
            tickingCountDown--;
        }
    }

    public void explodingImg() {
        if (explosionCountDown == 0) {
            setDone(true);
            this.img = null;
        } else {
            this.img = Sprite
                    .bombExplodeSprite(Sprite.bomb_exploded, Sprite.bomb_exploded1,
                            Sprite.bomb_exploded2, explosionCountDown)
                    .getFxImage();
            explosionCountDown--;
        }
    }

    public void handleFlameCollision(List<Entity> entities, List<Entity> staticObjects,
                                     List<Entity> damagedEntities) {

        // damage bricks
        staticObjects.stream().filter(b -> (b instanceof Brick)).forEachOrdered((Entity b) -> {
            flames.forEach((Entity flame) -> {
                int bX = (int) b.getX(), fX = (int) flame.getX(), x = (int) this.getX();
                int bY = (int) b.getY(), fY = (int) flame.getY(), y = (int) this.getY();
                String position;
                position = ((Flame) flame).getPos();
                
                if (!position.equals("left_most") && !position.equals("down_most") &&
                        !position.equals("right_most") && !position.equals("top_most")) {
                    if (bX == fX && x == bX && bY - fY == 1 ||
                            bX == fX && x == bX && bY - fY == -1 ||
                            bX - fX == -1 && bY == fY && y == bY ||
                            bX - fX == 1 && bY == fY && y == bY) {
                        ((Brick) b).setDamaged(true);
                        damagedEntities.add(b);
                    }
                } else {
                    if (bX == fX && bY == fY) {
                        ((Brick) b).setDamaged(true);
                        damagedEntities.add(b);
                    }
                }
            });
        });

        // damage entities
        entities.stream().filter(x -> (x instanceof Bomber || x instanceof Enemy || x instanceof Bomb)).forEachOrdered(x -> {
            flames.forEach(flame -> {
                if (flame.rectangle.intersects(x.rectangle.getLayoutBounds())) {
                    if (x instanceof Bomber) {
                        if(((Bomber) x).shield == false){
                            ((Bomber) x).setLive(false);
                            damagedEntities.add(x);
                        } else {
                            ((Bomber) x).setLive(true);
                        }
                    } else if (x instanceof Enemy) {
                        ((Enemy) x).setDamaged(true);
                        damagedEntities.add(x);
                    }
                    
                    // chain explosion
                    if (x instanceof Bomb) {
                        // if (((Bomb) x).getTickingCountDown() > 10)
                        ((Bomb) x).setTickingCountDown(0);
                        ((Bomb) x).getFlames().forEach(o -> {
                            ((Flame) o).setExplosionCountDown(15);
                        });
                    }
                }
            });
        });
    }

}


