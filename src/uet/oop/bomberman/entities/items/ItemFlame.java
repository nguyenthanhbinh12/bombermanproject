package uet.oop.bomberman.entities.items;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.Item;

public class ItemFlame extends Item {

    public ItemFlame(int x, int y, Image img) {
        super(x, y, img);
    }
}
