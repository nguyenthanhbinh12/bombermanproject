package uet.oop.bomberman.entities.items;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.Item;

public class ItemPortal extends Item {
    public ItemPortal(int x, int y, Image img) {
        super(x, y, img);
    }

}
