package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.Enemy;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sounds.PlaySound;

public class Oneal extends Enemy {

    public Oneal(int x, int y, Image img) {
        super(x, y, img);
        speed = 0.025;
        life = 1;
        throughWall = false;
        status.totalmove = 0;
        //shell = 1;
        //damaged = false;

    }


    @Override
    public boolean moveLeft() {
        img = Sprite
                .movingSprite(Sprite.oneal_left1, Sprite.oneal_left2, Sprite.oneal_left3, status.left)
                .getFxImage();
        if (status.left == 8) {
            status.left = 0;
            status.totalmove =0;
        } else {
            status.left++;
            status.totalmove++;
        }
        return super.moveLeft();

    }

    @Override
    public boolean moveDown() {
        img = Sprite
                .movingSprite(Sprite.oneal_right1, Sprite.oneal_right2, Sprite.oneal_right3, status.down)
                .getFxImage();
        if (status.down == 8) {
            status.down = 0;
            status.totalmove =0;
        } else {
            status.down++;
            status.totalmove++;
        }
        return super.moveDown();
    }

    @Override
    public boolean moveRight() {
        img = Sprite
                .movingSprite(Sprite.oneal_right1, Sprite.oneal_right2, Sprite.oneal_right3, status.right)
                .getFxImage();
        if (status.right == 8) {
            status.right = 0;
            status.totalmove = 0;
        } else {
            status.right++;
            status.totalmove++;
        }
        return super.moveRight();
    }

    @Override
    public boolean moveUp() {
        img = Sprite
                .movingSprite(Sprite.oneal_left1, Sprite.oneal_left2, Sprite.oneal_left3, status.up)
                .getFxImage();
        if (status.up == 8) {
            status.up = 0;
            status.totalmove =0 ;
        } else {
            status.up++;
            status.totalmove++;
        }
        return super.moveUp();
    }

    @Override
    public void dieImg() {
        if (deathCountDown == 0) {
            this.img = null;
        } else {
            this.img = Sprite
                    .dieSprite(Sprite.oneal_dead, Sprite.oneal_dead, Sprite.oneal_dead, deathCountDown)
                    .getFxImage();
            deathCountDown--;
            PlaySound.enedie.play();
        }
//        PlaySound.playBackgroundMusic(PlaySound.MAIN_MENU, true);
    }

//    @Override
//    public void updatespeed() {
//        if (status.totalmove < 8 && status.totalmove >=4 ) {
//            this.speed = 0.075;
//            //status.totalmove = 0;
//        }else if (status.totalmove >= 0 && status.totalmove <4 || status.totalmove == 8) {
//            this.speed = 0.025;
//        }
//       // super.update();
//    }
}
