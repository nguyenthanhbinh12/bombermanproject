package uet.oop.bomberman.entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.abstracts.Enemy;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sounds.PlaySound;

public class Balloon extends Enemy {
    public Balloon(int x, int y, Image img) {
        super(x, y, img);
        speed = 0.04;
        life = 1;
        throughWall = false;
        // rectangle = new Rectangle(x, y, 0.98, 0.98);
    }

    @Override
    public boolean moveLeft() {
        img = Sprite
                .movingSprite(Sprite.balloom_left1, Sprite.balloom_left2, Sprite.balloom_left3, status.left)
                .getFxImage();
        if (status.left == 8) {
            status.left = 0;
        } else {
            status.left++;
        }
        return super.moveLeft();

    }

    @Override
    public boolean moveDown() {
        img = Sprite
                .movingSprite(Sprite.balloom_right1, Sprite.balloom_right2, Sprite.balloom_right3, status.down)
                .getFxImage();
        if (status.down == 8) {
            status.down = 0;
        } else {
            status.down++;
        }
        return super.moveDown();
    }

    @Override
    public boolean moveRight() {
        img = Sprite
                .movingSprite(Sprite.balloom_right1, Sprite.balloom_right2, Sprite.balloom_right3, status.right)
                .getFxImage();
        if (status.right == 8) {
            status.right = 0;
        } else {
            status.right++;
        }
        return super.moveRight();
    }

    @Override
    public boolean moveUp() {
        img = Sprite
                .movingSprite(Sprite.balloom_left1, Sprite.balloom_left2, Sprite.balloom_left3, status.up)
                .getFxImage();
        if (status.up == 8) {
            status.up = 0;
        } else {
            status.up++;
        }
        return super.moveUp();
    }

    @Override
    public void dieImg() {
        if (deathCountDown == 0) {
            this.img = null;
        } else {
            PlaySound.enedie.play();
            this.img = Sprite
                    .dieSprite(Sprite.balloom_dead, Sprite.balloom_dead, Sprite.balloom_dead, deathCountDown)
                    .getFxImage();
            deathCountDown--;
        }
    }
}
