package uet.oop.bomberman.entities.abstracts;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import uet.oop.bomberman.graphics.Sprite;

public abstract class Entity {
     //T?a ?? X t�nh t? g�c tr�i tr�n trong Canvas
    protected double x;
     //T?a ?? Y t�nh t? g�c tr�i tr�n trong Canvas
    protected double y;

    protected Image img;
    public Rectangle rectangle;

    //Kh?i t?o ??i t??ng, chuy?n t? t?a ?? ??n v? sang t?a ?? trong canvas
    public Entity(double x, double y, Image img) {
        this.x = x;
        this.y = y;
        this.img = img;
    }
    

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public void render(GraphicsContext gc) {
        if (this.x >= 0 && this.y >= 0) {
            gc.drawImage(img, x * Sprite.SCALED_SIZE, y * Sprite.SCALED_SIZE);
        }
    }

    public boolean moveLeft() {
        return true;
    }

    ;

    public boolean moveRight() {
        return true;
    }

    ;

    public boolean moveUp() {
        return true;
    }

    ;

    public boolean moveDown() {
        return true;
    }

    ;

    public void setLocation(double x, double y) {
        rectangle.setX(x);
        rectangle.setY(y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public abstract void update();
}
