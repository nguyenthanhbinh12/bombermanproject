package uet.oop.bomberman.entities.abstracts;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

public abstract class Item extends Entity {
    private final boolean isCollision = false;

    public Item(int x, int y, Image img) {
        super(x, y, img);
        rectangle = new Rectangle(x, y, 0.98, 0.98);
    }


    //Collision Bomber

    public boolean collision(Entity entities) {
        return this.rectangle.intersects(entities.rectangle.getLayoutBounds());
    }

    @Override
    public void update() {

    }
}
